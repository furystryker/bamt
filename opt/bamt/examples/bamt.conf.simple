# Big A Miner Thing configuration
# /etc/bamt/bamt.conf
#
# When done editing, restart mining to make changes here take effect
# If you're just setting up BAMT, be sure to edit pools as well.
#
# As of 0.5, many less common options have been removed from this
# default config to make it easier on the noobs.
#
# You will find a complete example with all options described
# at /opt/bamt/examples/bamt.conf
#
# Note: This is a YAML file.  Indentation is significant.
#
# If you break things badly, a backup of this file in it's original
# form can be found at /opt/bamt/examples/bamt.conf.simple
#


---
settings:

  # miner_id is used to set hostname and identify this rig to munin and
  # other processes
  # IMPORTANT! miner_id must be a valid hostname: all lower case, no weird
  # characters, underscores, periods, or spaces. Dash and numbers are ok.

  miner_id: bamt-miner

  # loc is any additional string you wish to use to identify or group this rig

  miner_loc: unspecified location

  # should we monitor the cards? 1 = yes, 0 = no

  do_monitor: 0

  # if monitoring, how do we send email alerts?

  # these are required..

  # smtp_host: smtp.gmail.com
  # smtp_to: you@yourmail.com
  
  # these are optional..

  # smtp_from: somefrom@address
  # smtp_subject: I wanted a custom subject in my alerts...
  # smtp_port: 587
  # smtp_auth_user: user@where
  # smtp_auth_pass: secret
  # smtp_tls: 1
  # smtp_ssl: 1

  
  
# GPU Configurtation

# You'll need one gpuX: section per GPU installed.  Not sure what's installed,
# or the order?  Open a root shell and type:  aticonfig --list-adapters
# 
# You can delete gpu sections if you don't have that many cards.. or add more
# if you find a way to run them



gpu0:

  # remove disabled: or set it to 0 to actually use this card..

  disabled: 1

  
  # overclocking.. all are optional  

  # optional command to execute prior to overclocking
  # any valid command (or the path to a script) is fine

  # pre_oc_cmd:


  # (core/mem)_X = the value to set for this profile (0-2)
  # Profile 2 is used while mining, but setting values
  # in profile 2 that are *lower* than the values in the other
  # profiles is not allowed on some GPUs, so you have to adjust
  # the other profiles as well.
  # For instance, to get 300Mhz mem clock on my 5830s, I have
  # to set all 3 profiles to 300.


  # core_speed_0: 300
  # core_speed_1: 800
  # core_speed_2: 980
  
  # mem_speed_0: 300
  # mem_speed_1: 300
  # mem_speed_2: 300

  # core_voltage_0: 1.125
  # core_voltage_1: 1.125
  # core_voltage_2: 1.125000

  # recommend to set and forget fan, preferably at 100
  
  fan_speed: 100

  # optional command to execute after overclocking
  # post_oc_cmd:


  # Phoenix mining setup (see full example for how to use cgminer instead)
  # We strongly recommend you use phoenix at, least until you are familiar
  # with BAMT

  # kernel to use.. one of:  phatk phatk2 poclbm
  # if in doubt, use phatk2

  kernel: phatk2

  # phoenix style kernel params.  *do not* include DEVICE=x, otherwise
  # exactly same as any phoenix setup.  
  # if in doubt: BFI_INT VECTORS AGGRESSION=11
  # (though 9 on GPU0 often makes using the GUI more pleasant)

  kernel_params: BFI_INT VECTORS FASTLOOP=false AGGRESSION=9


  # Pool file

  # IMPORTANT: pool_file must contain a list of pool URLs.. one at least, more if
  # you like.  You can share the same file for all GPUs, or have one for each.
  # Miner will start with first URL in file, however if no shares are accepted
  # for any reason in (pool_timeout) seconds, the miner will move to next URL
  # in the file.  Wraps around to first URL when last one fails.
  # You can also set pool specific values, see the example pools file.
  # if in doubt: /etc/bamt/pools

  pool_file: /etc/bamt/pools

  # max time to allow phoenix to run without finding a share.. 
  # exceed this and we kill phoenix, start a new instance with
  # next line in pools file

  pool_timeout: 180



  # monitoring limits, used by email alerts and gpumon
  # an alert is generated if the current value goes below a 'lo'
  # or above a 'hi'.

  # GPU temp sensor reading (in C)
  monitor_temp_lo: 45
  monitor_temp_hi: 80

  # GPU load (in %)
  monitor_load_lo: 80

  # average hashrate (in mh/s)
  monitor_hash_lo: 125

  # fam rpm (in rpm)
  monitor_fan_lo: 2000

  # effective reject rate (in % of total)
  monitor_reject_hi: 2



# OK.. hopefully you get the idea.  Stubs for additional GPUs below.
# There are many additional paramters to help you deal with various
# scenarios, see /opt/bamt/examples and the docs (if we write some)


gpu1:
  disabled: 1

  # core_speed_0: 300
  # core_speed_1: 800
  # core_speed_2: 1060
  # mem_speed_0: 300
  # mem_speed_1: 300
  # mem_speed_2: 1120
  # core_voltage_0: 1.125
  # core_voltage_1: 1.125
  # core_voltage_2: 1.125000

  fan_speed: 100

  kernel: phatk2
  kernel_params: BFI_INT VECTORS FASTLOOP=false AGGRESSION=11

  pool_file: /etc/bamt/pools
  pool_timeout: 180

  monitor_temp_lo: 45
  monitor_temp_hi: 80
  monitor_load_lo: 80
  monitor_hash_lo: 125
  monitor_fan_lo: 2000
  monitor_reject_hi: 1


gpu2:
  disabled: 1

  # core_speed_0: 300
  # core_speed_1: 800
  # core_speed_2: 980
  # mem_speed_0: 300
  # mem_speed_1: 300
  # mem_speed_2: 300
  # core_voltage_0: 1.125
  # core_voltage_1: 1.125
  # core_voltage_2: 1.125000

  fan_speed: 100

  kernel: phatk2
  kernel_params: BFI_INT VECTORS FASTLOOP=false AGGRESSION=11

  pool_file: /etc/bamt/pools
  pool_timeout: 180

  monitor_temp_lo: 45
  monitor_temp_hi: 80
  monitor_load_lo: 80
  monitor_hash_lo: 125
  monitor_fan_lo: 2000
  monitor_reject_hi: 1



gpu3:
  disabled: 1

  # core_speed_0: 300
  # core_speed_1: 800
  # core_speed_2: 980
  # mem_speed_0: 300
  # mem_speed_1: 300
  # mem_speed_2: 300
  # core_voltage_0: 1.125
  # core_voltage_1: 1.125
  # core_voltage_2: 1.125000

  fan_speed: 100

  kernel: phatk2
  kernel_params: BFI_INT VECTORS FASTLOOP=false AGGRESSION=11

  pool_file: /etc/bamt/pools
  pool_timeout: 180

  monitor_temp_lo: 45
  monitor_temp_hi: 80
  monitor_load_lo: 80
  monitor_hash_lo: 125
  monitor_fan_lo: 2000
  monitor_reject_hi: 1


gpu4:
  disabled: 1

  # core_speed_0: 300
  # core_speed_1: 800
  # core_speed_2: 980
  # mem_speed_0: 300
  # mem_speed_1: 300
  # mem_speed_2: 300
  # core_voltage_0: 1.125
  # core_voltage_1: 1.125
  # core_voltage_2: 1.125000

  fan_speed: 100

  kernel: phatk2
  kernel_params: BFI_INT VECTORS FASTLOOP=false AGGRESSION=11

  pool_file: /etc/bamt/pools
  pool_timeout: 180

  monitor_temp_lo: 45
  monitor_temp_hi: 80
  monitor_load_lo: 80
  monitor_hash_lo: 125
  monitor_shares_lo: 1


gpu5:
  disabled: 1

  # core_speed_0: 300
  # core_speed_1: 800
  # core_speed_2: 980
  # mem_speed_0: 300
  # mem_speed_1: 300
  # mem_speed_2: 300
  # core_voltage_0: 1.125
  # core_voltage_1: 1.125
  # core_voltage_2: 1.125000

  fan_speed: 100

  kernel: phatk2
  kernel_params: BFI_INT VECTORS FASTLOOP=false AGGRESSION=11

  pool_file: /etc/bamt/pools
  pool_timeout: 180

  monitor_temp_lo: 45
  monitor_temp_hi: 80
  monitor_load_lo: 80
  monitor_hash_lo: 125
  monitor_shares_lo: 1


gpu6:
  disabled: 1

  # core_speed_0: 300
  # core_speed_1: 800
  # core_speed_2: 980
  # mem_speed_0: 300
  # mem_speed_1: 300
  # mem_speed_2: 300
  # core_voltage_0: 1.125
  # core_voltage_1: 1.125
  # core_voltage_2: 1.125000

  fan_speed: 100

  kernel: phatk2
  kernel_params: BFI_INT VECTORS FASTLOOP=false AGGRESSION=11

  pool_file: /etc/bamt/pools
  pool_timeout: 180

  monitor_temp_lo: 45
  monitor_temp_hi: 80
  monitor_load_lo: 80
  monitor_hash_lo: 125
  monitor_shares_lo: 1


gpu7:
  disabled: 1

  # core_speed_0: 300
  # core_speed_1: 800
  # core_speed_2: 980
  # mem_speed_0: 300
  # mem_speed_1: 300
  # mem_speed_2: 300
  # core_voltage_0: 1.125
  # core_voltage_1: 1.125
  # core_voltage_2: 1.125000

  fan_speed: 100

  kernel: phatk2
  kernel_params: BFI_INT VECTORS FASTLOOP=false AGGRESSION=11

  pool_file: /etc/bamt/pools
  pool_timeout: 180

  monitor_temp_lo: 45
  monitor_temp_hi: 80
  monitor_load_lo: 80
  monitor_hash_lo: 125
  monitor_shares_lo: 1

