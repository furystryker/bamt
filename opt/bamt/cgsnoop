#!/usr/bin/perl

#    This file is part of BAMT.
#
#    BAMT is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    BAMT is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with BAMT.  If not, see <http://www.gnu.org/licenses/>.

# with bugs fixed by gigavps

eval
{
 require '/opt/bamt/common.pl';
 require '/opt/bamt/sendstatus.pl';
};

if ($@)
{
  die "\n\n\nWe were unable to find or process a core BAMT library:\n\n" .
    $@ . "\n\n\nThe BAMT tools cannot function until the above error is corrected.\n\nMaybe this happened";
}

$SIG{__DIE__} = sub { &handleDeath(@_); };

use IO::Socket::INET;
use Proc::PID::File;



$verbose = 0;

if ($ARGV[0] eq "-v")
{
	$verbose = 1;
}

if (Proc::PID::File->running())
{
        # one at a time, gentlemen
	if ($verbose)
	{
		print "Another cgsnoop process is active.  Exiting.\n";
	}
        exit(0);
}


my $broadcast = 1;
my $broadcastport = 54545;
my $directip = "";
my $directport = 0;

if (-e '/etc/bamt/cgsnoop.conf')
{
	open(H, '</etc/bamt/cgsnoop.conf') or die "could not open cgsnoop.conf: $!";
	
	my @conf = <H>;
	close(H);
	
	foreach my $line (@conf)
	{
		if ($line =~ /^broadcast\:\s*(\d)/)
		{
			$broadcast = $1;
			
			if ($verbose)
			{
				print "set broadcast to $1\n";
			}
		}
		elsif ($line =~ /^direct\:\s*(.+)\:(\d+)/)
		{
			$directip = $1;
			$directport = $2;
			
			if ($verbose)
			{
				print "set direct to $1 : $2\n";
			}
		}
		elsif ($line =~ /^broadcastport\:\s*(\d+)/)
		{
			$broadcastport = $1;
			
			if ($verbose)
			{
				print "set broadcastport to $1\n";
			}
		}
		elsif ($line =~ /^([^#]+)\:(\d+),(.+),(.+)$/)
		{
			my $host = $1;
			my $port = $2;
			my $miner_id = $3;
			my $miner_loc = $4;
			
			if ($verbose)
			{
				print "gather status for $1 port $2, $3 at $4\n";
			}
			
			my $sock = new IO::Socket::INET (
                                  PeerAddr => $host,
                                  PeerPort => $port,
                                  Proto => 'tcp',
                                  ReuseAddr => 1,
                                  Timeout => 10,
                                 );
    
            if ($sock)
            {
            	print $sock "devs|\n";
    
            	my $res = "";
		
            	while(<$sock>) 
            	{
            		$res .= $_;
            	}

            	close($sock);
            	
            	if ($verbose)
            	{
            		print "result:\n$res\n";
            		print "pools:\n$pools\n";
            	}

            	my $msg = $miner_id . '|' . $miner_loc;
            	
            	while ($res =~ m/\|GPU=(\d+),.*?Temperature=([\d\.]+),Fan\sSpeed=(\d+),Fan\sPercent=(\d+),GPU\sClock=(\d+),Memory\sClock=(\d+),GPU\sVoltage=([\d\.]+),GPU\sActivity=(\d+),.*?,MHS\s5s=([\d\.]+),Accepted=(\d+),Rejected=(\d+)/g)
            	{
            		$msg .= '|' . $1 . ':{"fan_speed":"' . $4 . '","current_temp_0":"' . $2 . '","current_core_clock":"' . $5 . '","current_load":"' . $8 . '","hashrate":' . $9 . ',"shares_invalid":"' . $11 . '","fan_rpm":"' . $3 . '","shares_accepted":"' . $10 . '","pool_url":"cgsnoop/' . $host . '","desc":"unknown (cgsnoop)","current_mem_clock":"' . $6 . '","current_core_voltage":"' . $7 . '"}';
            			
            		if ($verbose)
            		{
            			print "GPU $1\n";
            		}
            	}

                while ($res =~ m/\|PGA=(\d+),.*?Temperature=([\d\.]+),.*?MHS\sav=([\d\.]+).*?Accepted=(\d+),Rejected=(\d+)/g)
                {
                        $msg .= '|' . $1 . ':{"fan_speed":"0","current_temp_0":"' . $2 . '","current_core_clock":"0","current_load":"99","hashrate":' . $3 . ',"shares_invalid":"' . $5 . '","fan_rpm":"0","shares_accepted":"' . $4 . '","pool_url":"cgsnoop/' . $host . '","desc":"unknown (cgsnoop)","current_mem_clock":"0","current_core_voltage":"0"}';

                        if ($verbose)
                        {
                                print "PGA $1\n";
                        }
                }


            	if ($verbose)
            	{
            		print "message:\n$msg\n\n";
            	}

            	# send it..
            	
            	if ($broadcast == 1)
            	{
            		 my $socket = IO::Socket::INET->new(Broadcast => 1, Blocking => 1, ReuseAddr => 1, Type => SOCK_DGRAM, Proto => 'udp', PeerPort => $broadcastport, LocalPort => 0, PeerAddr => inet_ntoa(INADDR_BROADCAST));
            		 $socket->send($msg, 0);
            		 close $socket;
            		 
            		 if ($verbose)
            		 {
            		 	 print "broadcast status of $miner_id on port $broadcastport\n";
            		 }
            	}
            	
            	if (($directip ne "") && ($directport != 0))
            	{
            		 my $socket = IO::Socket::INET->new(Blocking => 1, ReuseAddr => 1, Type => SOCK_DGRAM, Proto => 'udp', PeerPort => $directport, LocalPort => 0, PeerAddr => $directip);
            		 $socket->send($msg, 0);
            		 close $socket;
            		 
            		 if ($verbose)
            		 {
            		 	 print "direct status of $miner_id to $directip : $directport\n";
            		 }
            	}	
            }
            else
            {
            	if ($verbose)
            	{
            		print "unable to connect to $host : $port\n";
            	}
            }
        }
    }
}
else
{
	die "cgsnoop config not found (should be at /etc/bamt/cgsnoop.conf)";
}
