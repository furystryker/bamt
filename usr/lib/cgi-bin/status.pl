#!/usr/bin/perl
use CGI qw(:standard);
use Data::Dumper;

require '/opt/bamt/common.pl';

our $conf = &getConfig;
%conf = %{$conf};

$q=CGI->new();

$showgpu = -1;

$mgpumon = $q->param('mgpumon') or $mgpumon = "";

if (defined($q->param('gpu')))
{
	$showgpu = $q->param('gpu');
}

$refer = $q->referer();

if ($refer =~ m/.*\/mgpumon\/$/)
{
	$mgpumon = $refer;
}

my $url = "?";

if (! $mgpumon eq "")
{
	$url .= "mgpumon=$mgpumon&";
}

if ($showgpu > -1)
{
	$url .= "gpu=$showgpu&";
}

print header;
if ($url eq "?")
{
	print start_html( -title=>'BAMT - ' . $conf{'settings'}{'miner_id'} . ' status', -style=>{-src=>'/bamt/status.css'},  -head=>$q->meta({-http_equiv=>'REFRESH',-content=>'30'})  );
}
else
{
	$url .= "tok=1";
	print start_html( -title=>'BAMT - ' . $conf{'settings'}{'miner_id'} . ' status', -style=>{-src=>'/bamt/status.css'},  -head=>$q->meta({-http_equiv=>'REFRESH',-content=>'30; url=' . $url })  );
}

# pull gpu info
my @gpus = &getFreshGPUData(1);


#gather totals
my $tot_mhash = 0;
my $tot_accept = 0;
my $tot_invalid = 0;

my $gput = "";
my $problems = 0;
my $okgpus = 0;
my $problemgpus = 0;
my @nodemsg;
my @gpumsg;


$g1put .= "<TR class='ghdr'><TD class='ghdr'>GPU</TD>";
$g1put .= "<TD class='ghdr'>Status</TD>";
$g1put .= "<TD class='ghdr'>Temp</TD>";

$g1put .= "<TD class='ghdr'>Load</TD>";
$g1put .= "<TD class='ghdr'>Rate</TD>";
$g1put .= "<TD class='ghdr' colspan=2>Accept/Invalid</TD>";

$g1put .= "<TD class='ghdr'>GPU Family</TD>";
$g1put .= "<TD class='ghdr'>Fan\% (rpm)</TD>";
$g1put .= "<TD class='ghdr'>Core</TD>";
$g1put .= "<TD class='ghdr'>Memory</TD>";
$g1put .= "<TD class='ghdr'>Power</TD>";

my $gsput = "";

for (my $i=0;$i<@gpus;$i++)
{
	my $gput = "";
	
	$tot_mhash += ${@gpus[$i]}{hashrate};	
	$tot_accept += ${@gpus[$i]}{shares_accepted};	
	$tot_invalid += ${@gpus[$i]}{shares_invalid};	

	
	my $url = $gpus[$i]{'pool_url'};
	
    if ($url =~ m/.+\@(.+)/)
    {
      $url = $1;

      if ($url =~ m/(.+):.*/)
      {
         $url = $1;
      }
    }

    $gput .= '<td style="border: 0px; padding: 5px;">' . $url . '</TD></TR></TABLE></TD>';

    if ($i == $showgpu)
	{
		$gsput .= "<tr><td>Status:</td><td>$url</td></tr>";
		
		if ($conf{'gpu'. $i}{cgminer})
		{
			$gsput .= "<tr><td>Miner:</td><td>cgminer</td></tr>";
		}
		else
		{
			$gsput .= "<tr><td>Miner:</td><td>Phoenix</td></tr>";
		}
		
	}
    
	if (defined($conf{'gpu'. $i}{monitor_temp_hi}) && ($gpus[$i]{'current_temp_0'} > $conf{'gpu'. $i}{monitor_temp_hi}))
	{
			$problems++;
			push(@nodemsg, "GPU $i is over maximum temp");
			
			if ($i == $showgpu)
			{
				push(@gpumsg, "Over maximum temp");
				$gsput .= "<tr><td>Temp:</td><td class='error'>" . sprintf("%.1f", $gpus[$i]{'current_temp_0'}) . ' c</td></tr>';	
			}
			
			$gput .= "<td class='error'>";
	}
	elsif (defined( $conf{'gpu'. $i}{monitor_temp_lo}) && ($gpus[$i]{'current_temp_0'} < $conf{'gpu'. $i}{monitor_temp_lo}))
	{
			$problems++;
			push(@nodemsg, "GPU $i is below minimum temp");

			if ($i == $showgpu)
			{
				push(@gpumsg, "Below minimum temp");
				$gsput .= "<tr><td>Temp:</td><td class='error'>" . sprintf("%.1f", $gpus[$i]{'current_temp_0'}) . ' c</td></tr>';	
			}
			
			$gput .= "<td class='error'>";
	}
	else
	{
		if ($i == $showgpu)
		{
			$gsput .= "<tr><td>Temp:</td><td>" . sprintf("%.1f", $gpus[$i]{'current_temp_0'}) . ' c</td></tr>';	
		}
		$gput .= '<td>';
	}
	
	
	
	$gput .= sprintf("%.1f", $gpus[$i]{'current_temp_0'}) . ' c';

	$gput .= '</TD>';

	if (defined($conf{'gpu'. $i}{monitor_hash_lo}) && ($gpus[$i]{'current_load'} < $conf{'gpu'. $i}{monitor_load_lo}))
	{
		$problems++;
		push(@nodemsg, "GPU $i is below minimum load");
		
		if ($i == $showgpu)
		{
			push(@gpumsg, "Below minimum load");
			$gsput .= "<tr><td>Load:</td><td class='error'>" . $gpus[$i]{'current_load'}  ."%</td></tr>";	
		}
		
		$gput .= "<td class='error'>";
	}
	else
	{
		if ($i == $showgpu)
		{
			$gsput .= "<tr><td>Load:</td><td>" . $gpus[$i]{'current_load'}  ."%</td></tr>";	
		}
		
		$gput .= '<td>';
	}
	
	$gput .= $gpus[$i]{'current_load'} . '%</TD>';
	
	
	
	
	
	if (defined($conf{'gpu'. $i}{monitor_hash_lo}) && ($gpus[$i]{'hashrate'} < $conf{'gpu'. $i}{monitor_hash_lo}))
	{
		$problems++;
		push(@nodemsg, "GPU $i is below minimum hash rate");
		
		if ($i == $showgpu)
		{
			push(@gpumsg, "Below minimum hash rate");
			
		}

		
		$gput .= "<td class='error'>";
	}
	else
	{
		$gput .= '<td>';
	}
	
	$gput .= sprintf("%.2f", $gpus[$i]{'hashrate'}) . " Mh/s";
		
	$gput .= "</TD><TD>";
	$gput .= $gpus[$i]{'shares_accepted'} . " / " . $gpus[$i]{'shares_invalid'};
	
	$gput .= '</TD>';
	
	
	
	if ($gpus[$i]{'shares_accepted'} > 0)
	{
		my $rr = $gpus[$i]{'shares_invalid'}/($gpus[$i]{'shares_accepted'} + $gpus[$i]{'shares_invalid'})*100 ;
		
		if (defined(${$config}{monitor_reject_hi}) && ($rr > ${$config}{monitor_reject_hi}))
		{
			$problems++;
			push(@nodemsg, "GPU $i is above maximum reject rate");
			
			if ($i == $showgpu)
			{
				push(@gpumsg, "Above maximum reject rate");
				$gsput .= "<tr><td>Shares A/R:</td><td class='error'>" .  $gpus[$i]{'shares_accepted'} . ' / ' . $gpus[$i]{'shares_invalid'} . ' (' . sprintf("%-2.2f%", $rr) . ")</td></tr>";
			}
			
			$gput .= "<td class='error'>";
		}
		else
		{
			if ($i == $showgpu)
			{
				$gsput .= "<tr><td>Shares A/R:</td><td>" .  $gpus[$i]{'shares_accepted'} . ' / ' . $gpus[$i]{'shares_invalid'} . ' (' . sprintf("%-2.2f%", $rr) . ")</td></tr>";
			}
			
			$gput .= '<td>';
		}
		
		$gput .= sprintf("%-2.2f%", $rr);
	}
	else
	{
		if ($i == $showgpu)
		{
				$gsput .= "<tr><td>Shares A/R:</td><td>" .  $gpus[$i]{'shares_accepted'} . ' / ' . $gpus[$i]{'shares_invalid'} . "</td></tr>";
		}
		
		$gput .= '<td>n/a';
	}
	
	$gput .= "</TD><TD>";
	if ($gpus[$i]{'desc'} =~ m/.*\s(\d+\sSeries).*/i)
	{
		if ($i == $showgpu)
		{
				$gsput .= "<tr><td>GPU family:</td><td>" .  $1 . "</td></tr>";
		}
		$gput .= $1;
	}
	else
	{
		if ($i == $showgpu)
		{
				$gsput .= "<tr><td>GPU family:</td><td>" . $gpus[$i]{'desc'}  . "</td></tr>";
		}
		$gput .= $gpus[$i]{'desc'};
	}
	
	$gput .= '</TD>';
	
	if (defined($conf{'gpu'. $i}{monitor_fan_lo}) && ($gpus[$i]{'fan_rpm'} < $conf{'gpu'. $i}{monitor_fan_lo}) && (! $gpus[$i]{'fan_rpm'} eq 'na'))
	{
		$problems++;
		push(@nodemsg, "GPU $i is below minimum fan rpm");
		
		if ($i == $showgpu)
		{
			push(@gpumsg, "Below minimum fan rpm");
			$gsput .= "<tr><td>Fan speed:</td><td class='error'>" .  $gpus[$i]{'fan_speed'} . '% (' . $gpus[$i]{'fan_rpm'}  . " rpm)</td></tr>";
		}
		
		$gput .= "<td class='error'>";
	}
	else
	{
		if ($i == $showgpu)
		{
				$gsput .= "<tr><td>Fan speed:</td><td>" .  $gpus[$i]{'fan_speed'} . '% (' . $gpus[$i]{'fan_rpm'}  . " rpm)</td></tr>";
		}
		
		$gput .= '<td>';
	}
		
	$gput .= $gpus[$i]{'fan_speed'} . '% (' . $gpus[$i]{'fan_rpm'} . ')';
	
	$gput .= "</TD><TD>";
	
	$gput .= $gpus[$i]{'current_core_clock'} . ' Mhz';
	
	$gput .= "</TD><TD>";
	
	$gput .= $gpus[$i]{'current_mem_clock'} . ' Mhz';
	
	$gput .= "</TD><TD>";
	
	$gput .= $gpus[$i]{'current_core_voltage'} . 'v';

	if ($i == $showgpu)
	{
		$gsput .= "<tr><td>Core clock:</td><td>" . $gpus[$i]{'current_core_clock'} . ' Mhz</td></tr>'; 
		$gsput .= "<tr><td>Mem clock:</td><td>" . $gpus[$i]{'current_mem_clock'} . ' Mhz</td></tr>';
		$gsput .= "<tr><td>Core power:</td><td>" . $gpus[$i]{'current_core_voltage'} . "v</td></tr>";
	}
	
	$gput .= "</TD></TR>";
	
	my $gpuurl = "?";
	
	if (! $mgpumon eq "")
	{
		$gpuurl .= "mgpumon=$mgpumon&";
	}
	
	$gpuurl .= "gpu=$i";
	
	
	if ($problems)
	{
		$gput = '<TR><TD><font size=5><A href="' . $gpuurl . '">' . $i . '</TD><TD><table><tr><td style="border: 0px; padding: 5px;"><img src=/bamt/error24.png></td>' . $gput;
		$problemgpus++;
	}
	else
	{
		$gput = '<TR><TD><font size=5><A href="' . $gpuurl . '">' . $i . '</TD><TD><table><tr><td style="border: 0px; padding: 5px;"><img src=/bamt/ok24.png></td>' . $gput;
		$okgpus++;
	}
	
	$g1put .= $gput;
	$problems = 0;
}

print "<div id='overview'>";



print "<table><TR><TD id='overviewlogo'><IMG src='/bamt/bamt_small.png'></TD>";

print "<TD id='overviewhash'><b>" . $conf{'settings'}{'miner_id'} . "</b><br><font size=6>";
print sprintf("%.2f", $tot_mhash);
print " Mh/s</font></TD>";
print "<TD id='overviewshares'> $tot_accept total accepted shares<br>";
print" $tot_invalid total invalid shares<br>";
if ($tot_accept)
{
 print sprintf("%.3f%%", $tot_invalid / ($tot_accept + $tot_invalid)*100);
 print " reject ratio";
        
}


print "<TD id='overviewgpus'>";

print @gpus . "";
if (@gpus == 1)
{
	print " GPU configured<br>";
}
else
{
	print " GPUs configured<br>";
}

print $okgpus;
if ($okgpus == 1)
{
	print " GPU is OK<br>";
}
else
{
	print " GPUs are OK<br>";
}

print $problemgpus;
if ($problemgpus == 1)
{
	print " GPU has problems";
}
else
{
	print " GPUs have problems";
}

print "</td>";

if (! $mgpumon eq "")
{
	print "<td><A href=$mgpumon>Back to mgpumon..</A></td>";
}

print "</TR></table></div>";


print "<div id=content>";

if ($showgpu < 0)
{
	print "<div class='gpudata'>";
	
	print "<table>";
	
	print $g1put;
	
	print "</table>";
	
	print "</div>";
	
	print "<div id=gpugraphs>";
	
	print "<table id=graphs>";
	print "<tr><td>";
	
	my $img = $conf{'settings'}{'miner_id'} . '/' . $conf{'settings'}{'miner_id'} . '/gpuhash_all-day.png';
	
	if (-e '/tmp/munin/html/' . $img)
	{
		print "<img src='/munin/" . $img . "'>";
	}
	else
	{
		print "<font style='color: #999999; font-size: 10px;'>Hash summary graph not available yet.<br>It can take up to 10 minutes for graphs to be created<br>after a restart or node name change.";
	}
	
	print "</td><td>";
	
	my $img = $conf{'settings'}{'miner_id'} . '/' . $conf{'settings'}{'miner_id'} . '/gputemp_all-day.png';
	
	if (-e '/tmp/munin/html/' . $img)
	{
		print "<img src='/munin/" . $img . "'>";
	}
	else
	{
		print "<font style='color: #999999; font-size: 10px;'>Temperature summary graph not available yet.<br>It can take up to 10 minutes for graphs to be created<br>after a restart or node name change.";
	}
	
	print "</td></tr></table>";
	print "</div>";
	
	print "<P><A href='/munin/" . $conf{'settings'}{'miner_id'} . "/index.html'>More system stats (munin)...</A>";
}
else
{
	print "<div id='showgpu'>";

	print "<A HREF=?";
	
	if (! $mgpumon eq "")
	{
		$gpuurl .= "mgpumon=$mgpumon&";
	}
	
	print "tok=1> << Back to overview</A>";
	
	print "<P>";
	
	print "<table>";
	
	print "<tr><td id='showgpustats'>";
	
	print "<table><tr><td width=200px>";
	
	print "<font size=5>GPU $showgpu<br>";
	
	print sprintf("%.2f", $gpus[$showgpu]{'hashrate'}) . " Mh/s";	
	
	print "</font><P>";
	
	if (@gpumsg)
	{
		print "<img src='/bamt/error.png'><p>";
		
		foreach my $l (@gpumsg)
		{
			print "$l<br>";
		}
	}
	else
	{
		print "<img src='/bamt/ok.png'><p>";
		print "All parameters OK";
	}
	
	print "</td><td><table>$gsput</table></td></tr></table>";
	print "</td>";
	
	print "<td><img src='/munin/" .  $conf{'settings'}{'miner_id'} .'/'. $conf{'settings'}{'miner_id'} . "/gpuhash$showgpu-day.png'></td></tr>";
	
	print "<tr><td style='vertical-align: bottom;'><img src='/munin/" .  $conf{'settings'}{'miner_id'} .'/'. $conf{'settings'}{'miner_id'} . "/gputemp$showgpu-day.png'></td>";
	
	print "<td><img src='/munin/" .  $conf{'settings'}{'miner_id'} .'/'. $conf{'settings'}{'miner_id'} . "/gpushares$showgpu-day.png'></td></tr></table>";
	
	print "</div>";
}


print "</body></html>";

